package org.webswing.server.base;

public interface WebswingService{
	void start() throws WsInitException;

	void stop();

}
